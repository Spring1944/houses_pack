local defs = {}

defs["s44_b_house_4_5"] = {
    name         = "s44_b_house_4_5",
    world        ="All Worlds",
    description  ="House",
    category     ="Buildings",
    object       ="features/s44_b_house_4_5.dae",
    featureDead  = "s44_b_house_4_5_destroyed_1",
    footprintx   = 7,
    footprintz   = 6,
    height       = 78,
    blocking        = true,
    burnable        = false,
    reclaimable     = false,
    noSelect        = false,
    indestructible  = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    upright         = true,
    floating        = false,
    collisionvolumetype     = "box",
    collisionvolumescales   = {120, 78, 96},
    collisionvolumeoffsets  = {12, 0, 0},
    customparams = {
        author       = "S44", 
        category     = "building", 
        set          = "S44 Houses", 
 
        randomrotate = "true", 

        normaltex    = "unittextures/s44_b_house_4_1_normalmap.png",
    }, 
}

defs["s44_b_house_4_5_destroyed_1"] = {
    name         = "s44_b_house_4_5_destroyed_1",
    world        ="All Worlds",
    description  ="House",
    category     ="Buildings",
    object       ="features/s44_b_house_4_5_destroyed_1.dae",
    featureDead  = "s44_b_house_4_5_destroyed_2",
    footprintx   = 7,
    footprintz   = 6,
    height       = 52,
    blocking        = true,
    burnable        = false,
    reclaimable     = false,
    noSelect        = false,
    indestructible  = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    upright         = true,
    floating        = false,
    collisionvolumetype     = "box",
    collisionvolumescales   = {120, 52, 96},
    collisionvolumeoffsets  = {12, 0, 0},
    customparams = {
        author       = "S44", 
        category     = "building", 
        set          = "S44 Houses", 
 
        randomrotate = "true", 

        normaltex    = "unittextures/s44_b_house_4_1_normalmap.png",
    }, 
}


defs["s44_b_house_4_5_destroyed_2"] = {
    name         = "s44_b_house_4_5_destroyed_2",
    world        ="All Worlds",
    description  ="House",
    category     ="Buildings",
    object       ="features/s44_b_house_4_5_destroyed_2.dae",
    footprintx   = 7,
    footprintz   = 6,
    height       = 28,
    blocking        = false,
    burnable        = false,
    reclaimable     = false,
    noSelect        = false,
    indestructible  = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    upright         = true,
    floating        = false,
    collisionvolumetype     = "box",
    collisionvolumescales   = {120, 28, 96},
    collisionvolumeoffsets  = {12, 0, 0},
    customparams = {
        author       = "S44", 
        category     = "building", 
        set          = "S44 Houses", 
 
        randomrotate = "true", 

        normaltex    = "unittextures/s44_b_house_4_1_normalmap.png",
    }, 
}

return lowerkeys( defs )
