local defs = {}

defs["s44_b_house_1_6"] = {
    name         = "s44_b_house_1_6",
    world        ="All Worlds",
    description  ="House",
    category     ="Buildings",
    object       ="features/s44_b_house_1_6.dae",
    featureDead  = "s44_b_house_1_6_destroyed_1",
    footprintx   = 6,
    footprintz   = 9,
    height       = 56,
    blocking        = true,
    burnable        = false,
    reclaimable     = false,
    noSelect        = false,
    indestructible  = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    upright         = true,
    floating        = false,
    collisionvolumetype     = "box",
    collisionvolumescales   = {96, 56, 144},
    collisionvolumeoffsets  = {0, 0, 0},
    customparams = {
        author       = "S44", 
        category     = "building", 
        set          = "S44 Houses", 
 
        randomrotate = "true", 

        normaltex    = "unittextures/s44_b_house_1_1_normalmap.png",
    }, 
}

defs["s44_b_house_1_6_destroyed_1"] = {
    name         = "s44_b_house_1_6_destroyed_1",
    world        ="All Worlds",
    description  ="House",
    category     ="Buildings",
    object       ="features/s44_b_house_1_6_destroyed_1.dae",
    featureDead  = "s44_b_house_1_6_destroyed_2",
    footprintx   = 6,
    footprintz   = 9,
    height       = 48,
    blocking        = true,
    burnable        = false,
    reclaimable     = false,
    noSelect        = false,
    indestructible  = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    upright         = true,
    floating        = false,
    collisionvolumetype     = "box",
    collisionvolumescales   = {96, 48, 144},
    collisionvolumeoffsets  = {0, 0, 0},
    customparams = {
        author       = "S44", 
        category     = "building", 
        set          = "S44 Houses", 
 
        randomrotate = "true", 

        normaltex    = "unittextures/s44_b_house_1_1_normalmap.png",
    }, 
}


defs["s44_b_house_1_6_destroyed_2"] = {
    name         = "s44_b_house_1_6_destroyed_2",
    world        ="All Worlds",
    description  ="House",
    category     ="Buildings",
    object       ="features/s44_b_house_1_6_destroyed_2.dae",
    footprintx   = 6,
    footprintz   = 9,
    height       = 28,
    blocking        = false,
    burnable        = false,
    reclaimable     = false,
    noSelect        = false,
    indestructible  = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    upright         = true,
    floating        = false,
    collisionvolumetype     = "box",
    collisionvolumescales   = {96, 28, 144},
    collisionvolumeoffsets  = {0, 0, 0},
    customparams = {
        author       = "S44", 
        category     = "building", 
        set          = "S44 Houses", 
 
        randomrotate = "true", 

        normaltex    = "unittextures/s44_b_house_1_1_normalmap.png",
    }, 
}

return lowerkeys( defs )
