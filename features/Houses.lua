-- Default Spring Treedef

local defs = {}

defs["Fountain"] = {
    description    = "Fountain",
    object         = "Features/Fountain.dae",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    energy          = 0,
    damage          = 10000,
    metal           = 0,
    mass            = 10000,
    crushResistance = 16,
    footprintX  = 2,  -- 1 footprint unit = 16 elmo
    footprintZ  = 2,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "cylY",
    collisionVolumeScales = {32, 4, 32},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesConcrete_normals.png",
    },
}

defs["HouseMansion"] = {
    description    = "HouseMansion",
    object         = "Features/HouseMansion.dae",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    energy          = 0,
    damage          = 100000,
    metal           = 0,
    mass            = 10000,
    crushResistance = 1000,
    footprintX  = 11,  -- 1 footprint unit = 16 elmo
    footprintZ  = 10,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {168, 80, 149},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseMansion_normals.png",
    },
}

defs["HouseMedieval_001"] = {
    description    = "HouseMedieval_001",
    object         = "Features/MedievalHouse_001.dae",
    featureDead    = "HouseMedieval_001_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 4,  -- 1 footprint unit = 16 elmo
    footprintZ  = 3,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {69, 50, 40},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesMedievalHouse_normals.png",
    },
}
defs["HouseMedieval_001_destroyed_001"] = {
    description    = "HouseMedieval_001_destroyed_001",
    object         = "Features/MedievalHouse_001_destroyed_001.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 4,  -- 1 footprint unit = 16 elmo
    footprintZ  = 3,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {69, 24, 40},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesMedievalHouseBurnt_normals.png",
    },
}

defs["HouseMedieval_002"] = {
    description    = "HouseMedieval_002",
    object         = "Features/MedievalHouse_002.dae",
    featureDead    = "HouseMedieval_002_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 4,  -- 1 footprint unit = 16 elmo
    footprintZ  = 3,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {57, 44, 38},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesMedievalHouse_normals.png",
    },
}
defs["HouseMedieval_002_destroyed_001"] = {
    description    = "HouseMedieval_002_destroyed_001",
    object         = "Features/MedievalHouse_002_destroyed_001.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 4,  -- 1 footprint unit = 16 elmo
    footprintZ  = 3,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {57, 24, 38},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesMedievalHouseBurnt_normals.png",
    },
}

defs["HouseMedieval_003"] = {
    description    = "HouseMedieval_003",
    object         = "Features/MedievalHouse_003.dae",
    featureDead    = "HouseMedieval_003_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 5,  -- 1 footprint unit = 16 elmo
    footprintZ  = 3,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {69, 41, 44},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesMedievalHouse_normals.png",
    },
}
defs["HouseMedieval_003_destroyed_001"] = {
    description    = "HouseMedieval_003_destroyed_001",
    object         = "Features/MedievalHouse_003_destroyed_001.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 5,  -- 1 footprint unit = 16 elmo
    footprintZ  = 3,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {69, 24, 44},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesMedievalHouseBurnt_normals.png",
    },
}

defs["HouseMedieval_004"] = {
    description    = "HouseMedieval_004",
    object         = "Features/MedievalHouse_004.dae",
    featureDead    = "HouseMedieval_004_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 4,  -- 1 footprint unit = 16 elmo
    footprintZ  = 3,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {65, 41, 44},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesMedievalHouse_normals.png",
    },
}
defs["HouseMedieval_004_destroyed_001"] = {
    description    = "HouseMedieval_004_destroyed_001",
    object         = "Features/MedievalHouse_004_destroyed_001.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 4,  -- 1 footprint unit = 16 elmo
    footprintZ  = 3,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {65, 24, 44},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesMedievalHouseBurnt_normals.png",
    },
}

defs["Factory_SmokeStack"] = {
    description    = "Factory_SmokeStack",
    object         = "Features/Factory_SmokeStack.dae",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    energy          = 0,
    damage          = 300000,
    metal           = 0,
    mass            = 30000,
    crushResistance = 3000,
    footprintX  = 3,  -- 1 footprint unit = 16 elmo
    footprintZ  = 3,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "cylY",
    collisionVolumeScales = {34, 212, 34},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesFactorySmokestack_normals.png",
    },
}
defs["Factory_001"] = {
    description    = "Factory_001",
    object         = "Features/Factory_001.dae",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    energy          = 0,
    damage          = 300000,
    metal           = 0,
    mass            = 30000,
    crushResistance = 3000,
    footprintX  = 7,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {100, 51, 53},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesFactory_normals.png",
    },
}
defs["Factory_002"] = {
    description    = "Factory_002",
    object         = "Features/Factory_002.dae",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    energy          = 0,
    damage          = 300000,
    metal           = 0,
    mass            = 30000,
    crushResistance = 3000,
    footprintX  = 8,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {122, 100, 86},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesFactory_normals.png",
    },
}
defs["Factory_003"] = {
    description    = "Factory_003",
    object         = "Features/Factory_003.dae",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    energy          = 0,
    damage          = 300000,
    metal           = 0,
    mass            = 30000,
    crushResistance = 3000,
    footprintX  = 8,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {122, 100, 86},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesFactory_normals.png",
    },
}

--------------------------------------------------------------------------------
-- Autogenerated houses
--------------------------------------------------------------------------------

defs["House_001"] = {
    description    = "House_001",
    object         = "Features/House_001.dae",
    featureDead    = "House_001_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 8,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {112, 109, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_167.png",
    },
}

defs["House_001_destroyed_001"] = {
    description    = "House_001_destroyed_001",
    object         = "Features/House_001_destroyed_001.dae",
    featureDead    = "House_001_destroyed_002",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 8,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {112, 48, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_167.png",
    },
}

defs["House_001_destroyed_002"] = {
    description    = "House_001_destroyed_002",
    object         = "Features/House_001_destroyed_002.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 8,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {112, 24, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_167.png",
    },
}

defs["House_002"] = {
    description    = "House_002",
    object         = "Features/House_002.dae",
    featureDead    = "House_002_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 9,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {136, 122, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_050.png",
    },
}

defs["House_002_destroyed_001"] = {
    description    = "House_002_destroyed_001",
    object         = "Features/House_002_destroyed_001.dae",
    featureDead    = "House_002_destroyed_002",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 9,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {136, 96, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_050.png",
    },
}

defs["House_002_destroyed_002"] = {
    description    = "House_002_destroyed_002",
    object         = "Features/House_002_destroyed_002.dae",
    featureDead    = "House_002_destroyed_003",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 9,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {136, 72, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_050.png",
    },
}

defs["House_002_destroyed_003"] = {
    description    = "House_002_destroyed_003",
    object         = "Features/House_002_destroyed_003.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 9,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {136, 24, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_050.png",
    },
}

defs["House_003"] = {
    description    = "House_003",
    object         = "Features/House_003.dae",
    featureDead    = "House_003_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    energy          = 0,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 83, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_007.png",
    },
}

defs["House_003_destroyed_001"] = {
    description    = "House_003_destroyed_001",
    object         = "Features/House_003_destroyed_001.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 24, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_007.png",
    },
}

defs["House_004"] = {
    description    = "House_004",
    object         = "Features/House_004.dae",
    featureDead    = "House_004_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {136, 110, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_179.png",
    },
}

defs["House_004_destroyed_001"] = {
    description    = "House_004_destroyed_001",
    object         = "Features/House_004_destroyed_001.dae",
    featureDead    = "House_004_destroyed_002",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 9,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {136, 72, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_179.png",
    },
}

defs["House_004_destroyed_002"] = {
    description    = "House_004_destroyed_002",
    object         = "Features/House_004_destroyed_002.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 9,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {136, 24, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_179.png",
    },
}

defs["House_005"] = {
    description    = "House_005",
    object         = "Features/House_005.dae",
    featureDead    = "House_005_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 102, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_042.png",
    },
}

defs["House_005_destroyed_001"] = {
    description    = "House_005_destroyed_001",
    object         = "Features/House_005_destroyed_001.dae",
    featureDead    = "House_005_destroyed_002",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 72, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_042.png",
    },
}

defs["House_005_destroyed_002"] = {
    description    = "House_005_destroyed_002",
    object         = "Features/House_005_destroyed_002.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 24, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_042.png",
    },
}

defs["House_006"] = {
    description    = "House_006",
    object         = "Features/House_006.dae",
    featureDead    = "House_006_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 8,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {112, 80, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_113.png",
    },
}

defs["House_006_destroyed_001"] = {
    description    = "House_006_destroyed_001",
    object         = "Features/House_006_destroyed_001.dae",
    featureDead    = "House_006_destroyed_002",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 8,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {112, 48, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_113.png",
    },
}

defs["House_006_destroyed_002"] = {
    description    = "House_006_destroyed_002",
    object         = "Features/House_006_destroyed_002.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 8,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {112, 24, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_113.png",
    },
}

defs["House_007"] = {
    description    = "House_007",
    object         = "Features/House_007.dae",
    featureDead    = "House_007_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 152, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_099.png",
    },
}

defs["House_007_destroyed_001"] = {
    description    = "House_007_destroyed_001",
    object         = "Features/House_007_destroyed_001.dae",
    featureDead    = "House_007_destroyed_002",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 152, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_099.png",
    },
}

defs["House_007_destroyed_002"] = {
    description    = "House_007_destroyed_002",
    object         = "Features/House_007_destroyed_002.dae",
    featureDead    = "House_007_destroyed_003",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 96, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_099.png",
    },
}

defs["House_007_destroyed_003"] = {
    description    = "House_007_destroyed_003",
    object         = "Features/House_007_destroyed_003.dae",
    featureDead    = "House_007_destroyed_004",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 48, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_099.png",
    },
}

defs["House_007_destroyed_004"] = {
    description    = "House_007_destroyed_004",
    object         = "Features/House_007_destroyed_004.dae",
    featureDead    = "House_007_destroyed_005",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 24, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_099.png",
    },
}

defs["House_007_destroyed_005"] = {
    description    = "House_007_destroyed_005",
    object         = "Features/House_007_destroyed_005.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 24, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_099.png",
    },
}

defs["House_008"] = {
    description    = "House_008",
    object         = "Features/House_008.dae",
    featureDead    = "House_008_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 9,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {136, 92, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_191.png",
    },
}

defs["House_008_destroyed_001"] = {
    description    = "House_008_destroyed_001",
    object         = "Features/House_008_destroyed_001.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 9,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {136, 24, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_191.png",
    },
}

defs["House_009"] = {
    description    = "House_009",
    object         = "Features/House_009.dae",
    featureDead    = "House_009_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 9,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {136, 74, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_043.png",
    },
}

defs["House_009_destroyed_001"] = {
    description    = "House_009_destroyed_001",
    object         = "Features/House_009_destroyed_001.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 9,  -- 1 footprint unit = 16 elmo
    footprintZ  = 6,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {136, 74, 88},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_043.png",
    },
}


defs["House_010"] = {
    description    = "House_010",
    object         = "Features/House_010.dae",
    featureDead    = "House_010_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 9,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {136, 84, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_127.png",
    },
}

defs["House_010_destroyed_001"] = {
    description    = "House_010_destroyed_001",
    object         = "Features/House_010_destroyed_001.dae",
    featureDead    = "House_010_destroyed_002",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 5,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {68, 84, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_127.png",
    },
}

defs["House_010_destroyed_002"] = {
    description    = "House_010_destroyed_002",
    object         = "Features/House_010_destroyed_002.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 9,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {136, 24, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_127.png",
    },
}

defs["House_011"] = {
    description    = "House_011",
    object         = "Features/House_011.dae",
    featureDead    = "House_011_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 75, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_166.png",
    },
}


defs["House_011_destroyed_001"] = {
    description    = "House_011_destroyed_001",
    object         = "Features/House_011_destroyed_001.dae",
    featureDead    = "House_011_destroyed_002",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 48, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_166.png",
    },
}

defs["House_011_destroyed_002"] = {
    description    = "House_011_destroyed_002",
    object         = "Features/House_011_destroyed_002.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 24, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_166.png",
    },
}

defs["House_012"] = {
    description    = "House_012",
    object         = "Features/House_012.dae",
    featureDead    = "House_012_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 83, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_098.png",
    },
}

defs["House_012_destroyed_001"] = {
    description    = "House_012_destroyed_001",
    object         = "Features/House_012_destroyed_001.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 6,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {88, 24, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_098.png",
    },
}

defs["House_013"] = {
    description    = "House_013",
    object         = "Features/House_013.dae",
    featureDead    = "House_013_destroyed_001",
    blocking       = true,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = false,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 8,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {112, 91, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_008.png",
    },
}

defs["House_013_destroyed_001"] = {
    description    = "House_013_destroyed_001",
    object         = "Features/House_013_destroyed_001.dae",
    blocking       = false,
    burnable       = false,
    reclaimable    = false,
    noSelect       = false,
    indestructible = true,
    damage          = 6250,
    crushResistance = 5000,
    energy          = 0,
    metal           = 0,
    footprintX  = 8,  -- 1 footprint unit = 16 elmo
    footprintZ  = 4,  -- 1 footprint unit = 16 elmo
    upright =  true,
    floating = false,
    collisionVolumeTest = 1,
    collisionVolumeType = "box",
    collisionVolumeScales = {112, 24, 64},
    collisionVolumeOffsets = {0, 0, 0},
    customParams = {
        mod       = true,
        normaltex = "unittextures/FeaturesHouseNormal_008.png",
    },
}


return lowerkeys( defs )
